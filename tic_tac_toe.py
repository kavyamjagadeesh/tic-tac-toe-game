board = [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]
turn = 'X'
play_num = 0


def tic_tac_toe():
    global turn, play_num
    print "Player " + turn + " needs to play"
    play_num += 1
    row = 0
    col = 0
    while 1:
        while 1:
            print "Enter row number from top to bottom:"
            while 1:
                try:
                    row = int(raw_input())
                    break
                except ValueError:
                    print "Please enter only numbers between 1 and 3. Try again."
            if row in [1, 2, 3]:
                break
            else:
                print "Row number does not exist. Try again."

        while 1:
            print "Enter col number from left to right:"
            while 1:
                try:
                    col = int(raw_input())
                    break
                except ValueError:
                    print "Please enter only numbers between 1 and 3. Try again."
            if col in [1, 2, 3]:
                break
            else:
                print "Col number does not exist. Try again."

        if board[row - 1][col - 1] == " ":
            board[row - 1][col - 1] = turn
            break
        else:
            print "That spot is already taken. Try again"

    print_board(board)

    if play_num >= 5:  # cannot have a winner before that
        if (((board[row-1][0] == turn) and (board[row-1][1] == turn) and (board[row-1][2] == turn)) or
            ((board[0][col-1] == turn) and (board[1][col-1] == turn) and (board[2][col-1] == turn)) or
            ((board[0][0] == turn) and (board[1][1] == turn) and (board[2][2] == turn)) or
                ((board[0][2] == turn) and (board[1][1] == turn) and (board[2][0] == turn))):
            print "Game over: " + turn + " is the Winner!"
            exit()

    if turn == 'X':
        turn = 'O'
    else:
        turn = 'X'

    check = 0
    for row in board:
        if " " not in row:
            check += 1

    if check == 3:
        print "Game Over: Tie"
        exit()


def print_board(board1):
    print "Board: "
    for x, y, z in board1:  
        print " |" + x + "|" + y + "|" + z + "|"


# Execution starts here
print "Let's play Tic Tac Toe!"
print "Press STOP at any time to exit the game!"
print_board(board)
while 1:
    tic_tac_toe()